import HttpService from '../utils/HttpService'

export default class UserService {
    static decrementQuota = () => HttpService.post('/users/decrement-quota')

    static getFreshestProfile = () => HttpService.get('/users/me')

    static findById = (userId) => HttpService.get(`/users/${userId}`)
}
