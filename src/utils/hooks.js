import { useCallback, useRef, useEffect } from 'react'
import { useDispatch, useSelector, useStore } from 'react-redux'

/* eslint-disable */

export const useSelectorWithUpdate = (selector, actionType) => () => {
    const value = useSelector(selector)
    const dispatch = useDispatch()

    const update =
        actionType &&
        useCallback(
            (v) => {
                dispatch({
                    type: actionType,
                    payload: v,
                })
            },
            [dispatch]
        )

    return [value, update]
}

export const useAction = (action) => {
    const store = useStore()

    return useCallback(
        (...args) => action(...args)(store.dispatch, store.getState),
        [action, store]
    )
}

// https://overreacted.io/making-setinterval-declarative-with-react-hooks/
export function useInterval(callback, delay) {
    const savedCallback = useRef()

    useEffect(() => {
        savedCallback.current = callback
    }, [callback])

    useEffect(() => {
        function tick() {
            savedCallback.current()
        }
        if (delay !== null) {
            let id = setInterval(tick, delay)
            return () => clearInterval(id)
        }
    }, [delay])
}

/* eslint-enable */
