export const callSequentialTask = (tasksParam = [], task) =>
    tasksParam.reduce(
        (acc, currentValue) => acc.then(() => task(currentValue)),
        Promise.resolve()
    )
