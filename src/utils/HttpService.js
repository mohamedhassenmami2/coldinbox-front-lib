import { selectAccessToken } from '../store/domains/auth/auth.selectors'

const baseApiEnpoint = (env => {
    switch (env) {
        case 'development':
            return 'http://localhost:3030'
        case 'staging':
            return 'https://staging-coldinbox.herokuapp.com'
        case 'production':
        default:
            return 'https://api.coldinbox.com'
    }
})(process.env.NODE_ENV)

const getAuthorization = () => {
    if (!global.store) return {}

    const accessToken = selectAccessToken(global.store.getState())

    if (!accessToken) return {}

    return {
        Authorization: `Bearer ${accessToken}`,
    }
}

export default class HttpService {
    static get = apiEndpoint =>
        window
            .fetch(`${baseApiEnpoint}${apiEndpoint}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorization(),
                },
            })
            .then(res => {
                if (res.status >= 200 && res.status < 300) return res.json()

                return res.json().then(
                    err => Promise.reject(new Error(err.message)),
                    () => Promise.reject(res.statusText)
                )
            })

    static post = (apiEndpoint, body) =>
        window
            .fetch(`${baseApiEnpoint}${apiEndpoint}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorization(),
                },
                body: JSON.stringify(body),
            })
            .then(res => {
                if (res.status >= 200 && res.status < 300) return res.json()

                return res.json().then(
                    err => Promise.reject(new Error(err.message)),
                    () => Promise.reject(res.statusText)
                )
            })
}
