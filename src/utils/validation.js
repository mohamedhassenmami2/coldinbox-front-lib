export const emailValidation = {
    required: 'Required',
    pattern: {
        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
        message: 'invalid email address',
    },
}

export const pwdValidation = {
    required: 'Required',
    pattern: {
        value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$/,
        message: 'between 8 and 15 characters, 1 Uppercase letter, 1 Number',
    },
}
