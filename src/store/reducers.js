import { combineReducers } from 'redux'
import campaign from './domains/campaign/campaign.reducer'
import user from './domains/user/user.reducer'
import plan from './domains/plan/plan.reducer'
import meta from './domains/meta/meta.reducer'
import event from './domains/event/event.reducer'

export default combineReducers({
    campaign,
    user,
    plan,
    meta,
    event,
})
