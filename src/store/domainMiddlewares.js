import userMiddlewares from './domains/user/user.middlewares'
import metaMiddlewares from './domains/meta/meta.middlewares'
import eventMiddlewares from './domains/event/event.middlewares'

const allMiddlewares = [
    ...userMiddlewares,
    ...metaMiddlewares,
    ...eventMiddlewares,
]

export default (origin) =>
    allMiddlewares.map((middleware) => (store) => middleware(store, origin))
