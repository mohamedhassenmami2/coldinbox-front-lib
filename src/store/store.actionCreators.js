export const resetAll = () => (dispatch, _) =>
    dispatch({
        type: 'RESET',
    })

export const forceStoreUpdate = () => (dispatch, _) =>
    dispatch({
        type: 'FORCE_STORE_UPDATE',
    })
