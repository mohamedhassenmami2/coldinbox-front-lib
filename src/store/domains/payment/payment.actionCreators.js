import { selectUser } from '../user/user.selectors'
import PaymentService from '../../../clientApi/PaymentService'
import { updateUser } from '../user/user.actionCreators'
import { fetchAllPlans } from '../plan/plan.actionCreators'

export const applyCoupon = couponId => async (dispatch, getState) => {
    const user = selectUser(getState())

    const newUser = await PaymentService.applyCoupon(user.id, couponId)

    updateUser(newUser)(dispatch, getState)
    fetchAllPlans()(dispatch, getState)

    return newUser
}
