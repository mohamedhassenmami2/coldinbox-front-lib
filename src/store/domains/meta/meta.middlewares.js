import {
    RESYNC_STORE_WITH_STORAGE,
    INCREMENT_APP_VERSION,
} from './meta.actionTypes'
import { incrVersion } from './meta.actionCreators'
import { invokeActionCreator } from '../../../utils/reduxUtils'
import { selectCurrentPlanMessageQuota } from '../user/user.selectors'
import {
    POST_RAW_EVENT,
    WAIT_EVENT,
    SUCCESSFUL_SENT_INVITATION_TO_PROFILE,
    ONBOARDING_EXPLAIN_WAIT,
    CLOSE_TAB_PROFILE,
    FINISHED_CONTACTED_PROFILE,
    ERROR_CAMPAIGN_STOPPED,
} from '../event/event.actionTypes'
import { APP_SIDE } from '../../../../../../chrome/extension/constants'
import { setBadgeTextAndBackgroundColor } from '../../../../../chromeExtensionUtils/browserActions'
import {
    SET_ALL_CAMPAIGNS_STATUS_STOPPED,
    SET_CAMPAIGN_STATUS_FINISHED,
} from '../campaign/campaign.actionTypes'

const incrVersionOnRelevantChange = (store) => (next) => async (action) => {
    switch (action.type) {
        case RESYNC_STORE_WITH_STORAGE:
        case INCREMENT_APP_VERSION: {
            return next(action)
        }
        default: {
            invokeActionCreator(store)(incrVersion)()
        }
    }
    return next(action)
}

const updateBadgeOnAppState = (store, origin) => (next) => (action) => {
    const r = next(action)

    if (origin === APP_SIDE.POPUP) return r

    switch (action.type) {
        case POST_RAW_EVENT: {
            switch (action.payload.type) {
                case ONBOARDING_EXPLAIN_WAIT:
                case WAIT_EVENT: {
                    setBadgeTextAndBackgroundColor({
                        text: 'pause',
                        color: '#5f5e5e',
                    })

                    break
                }

                case ERROR_CAMPAIGN_STOPPED: {
                    setBadgeTextAndBackgroundColor({
                        text: 'Error',
                        color: 'red',
                    })

                    break
                }

                case CLOSE_TAB_PROFILE:
                case FINISHED_CONTACTED_PROFILE:
                case SUCCESSFUL_SENT_INVITATION_TO_PROFILE: {
                    // do nothing as it is messing with stopping campaign
                    break
                }
                default: {
                    const creditsBadgeText = selectCurrentPlanMessageQuota(
                        store.getState()
                    )

                    setBadgeTextAndBackgroundColor({
                        text: creditsBadgeText,
                        color: 'green',
                    })
                }
            }
            break
        }
        case SET_CAMPAIGN_STATUS_FINISHED:
        case SET_ALL_CAMPAIGNS_STATUS_STOPPED: {
            setBadgeTextAndBackgroundColor({
                text: '',
                color: '',
            })
            break
        }
        default: {
        }
    }

    return r
}

export default [incrVersionOnRelevantChange, updateBadgeOnAppState]
