import {
    INCREMENT_APP_VERSION,
    RESYNC_STORE_WITH_STORAGE,
} from './meta.actionTypes'

export const initialState = {
    appVersion: 0,
}

export default function versionReducer(state = initialState, action) {
    switch (action.type) {
        case RESYNC_STORE_WITH_STORAGE:
            return {
                ...action.payload.meta,
            }

        case INCREMENT_APP_VERSION:
            return {
                appVersion: state.appVersion + 1,
            }

        default:
            return state
    }
}
