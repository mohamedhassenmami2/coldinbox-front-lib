import {
    LAUNCH_NEW_CAMPAIGN,
    SET_PROFILES,
    SET_HAS_CONTACTED_PROFILE,
    SET_CAMPAIGN_STATUS_FINISHED,
    SET_ALL_CAMPAIGNS_STATUS_STOPPED,
    SET_HAS_LOGGED_PROFILE,
    SET_PROFILE_PERSONAL_INFO,
} from './campaign.actionTypes'
import ImmutableArray from '../../../utils/ImmutableArray'
import unionWith from 'lodash/unionWith'
import { PROFILE_STATUS, CAMPAIGN_STATUS } from './campaigns.constants'
import {
    RESYNC_STORE_WITH_STORAGE,
    RESET_ALL_STATES,
} from '../meta/meta.actionTypes'

export const initialState = {
    campaigns: [],
}

/**
 * @param {*} state
 *  message: string
 *  searchUrl: string
 *  profiles: Array
 *  status: enum<CAMPAIGN_STATUS>
 */
export default function messageReducer(state = initialState, action) {
    switch (action.type) {
        case LAUNCH_NEW_CAMPAIGN: {
            const { newCampaign } = action.payload || {}

            return {
                ...state,
                campaigns: ImmutableArray.push(state.campaigns, {
                    ...newCampaign,
                    status: CAMPAIGN_STATUS.RUNNING,
                }),
            }
        }

        case SET_PROFILES: {
            const { campaigns } = state
            const { profiles, campaignId } = action.payload

            const prevCampaign = campaigns[campaignId]
            const prevVisitedProfiles = prevCampaign.profiles || []
            const nextVisitedProfiles = profiles.map((url) => ({
                url,
                status: PROFILE_STATUS.CREATED,
            }))

            const mergedProfiles = unionWith(
                prevVisitedProfiles,
                nextVisitedProfiles,
                (a, b) => a.url === b.url
            )

            return {
                ...state,
                campaigns: ImmutableArray.replace(campaigns, campaignId, {
                    ...prevCampaign,
                    profiles: mergedProfiles,
                }),
            }
        }

        case SET_HAS_CONTACTED_PROFILE: {
            const { profile: targetProfile, campaignId } = action.payload
            const { campaigns } = state

            const prevCampaign = campaigns[campaignId]
            const prevProfiles = prevCampaign.profiles || []

            return {
                ...state,
                campaigns: ImmutableArray.replace(campaigns, campaignId, {
                    ...prevCampaign,
                    profiles: prevProfiles.map((profile) => {
                        if (profile.url === targetProfile.url) {
                            return {
                                ...profile,
                                status: PROFILE_STATUS.CONTACTED,
                                contactedDate: new Date(),
                            }
                        }

                        return profile
                    }),
                }),
            }
        }

        case SET_HAS_LOGGED_PROFILE: {
            const { profile: targetProfile, campaignId } = action.payload
            const { campaigns } = state

            const prevCampaign = campaigns[campaignId]
            const prevProfiles = prevCampaign.profiles || []

            return {
                ...state,
                campaigns: ImmutableArray.replace(campaigns, campaignId, {
                    ...prevCampaign,
                    profiles: prevProfiles.map((profile) => {
                        if (profile.url === targetProfile.url) {
                            return {
                                ...profile,
                                logged: true,
                            }
                        }

                        return profile
                    }),
                }),
            }
        }

        case SET_PROFILE_PERSONAL_INFO: {
            const { profileUrl, campaignId, profileInfo } = action.payload
            const { campaigns } = state

            const prevCampaign = campaigns[campaignId]
            const prevProfiles = prevCampaign.profiles || []

            return {
                ...state,
                campaigns: ImmutableArray.replace(campaigns, campaignId, {
                    ...prevCampaign,
                    profiles: prevProfiles.map((profile) => {
                        if (profile.url === profileUrl) {
                            return {
                                ...profile,
                                logged: true,
                                profileInfo,
                            }
                        }

                        return profile
                    }),
                }),
            }
        }

        case SET_CAMPAIGN_STATUS_FINISHED: {
            const { campaignId } = action.payload
            const { campaigns } = state

            const prevCampaign = campaigns[campaignId]

            return {
                ...state,
                campaigns: ImmutableArray.replace(campaigns, campaignId, {
                    ...prevCampaign,
                    status: CAMPAIGN_STATUS.FINISHED,
                }),
            }
        }

        case SET_ALL_CAMPAIGNS_STATUS_STOPPED: {
            const { campaigns } = state

            return {
                ...state,
                campaigns: campaigns.map((prevCampaign) => ({
                    ...prevCampaign,
                    status: CAMPAIGN_STATUS.STOPPED,
                })),
            }
        }

        case RESYNC_STORE_WITH_STORAGE: {
            return {
                ...action.payload.campaign,
            }
        }

        case RESET_ALL_STATES: {
            return initialState
        }

        default:
            return state
    }
}
