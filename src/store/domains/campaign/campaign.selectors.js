import { createSelector } from 'reselect'
import { flatMap, some, reduce, last } from 'lodash'
import { PROFILE_STATUS, CAMPAIGN_STATUS } from './campaigns.constants'

export const selectAllCampaigns = (store) => store.campaign.campaigns

export const selectCampaign = (store, props) => {
    const {
        campaign: { campaigns },
    } = store

    return campaigns[props.campaignId] || last(campaigns) || {}
}

export const selectProfile = createSelector(
    selectCampaign,
    (_, props) => props,
    (campaign, props) => {
        const { profiles } = campaign || { profiles: [] }

        const matchingProfile = profiles.filter(
            ({ url }) => url === props.profile.url
        )[0]

        return matchingProfile || last(profiles) || {}
    }
)

export const selectMostRecentCampaignId = (store) =>
    store.campaign.campaigns.length - 1

/**
 * selectMessage formatted message
 * @param campaignId
 * @param profile
 */
export const selectMessage = createSelector(
    selectCampaign,
    selectProfile,
    (campaign, profile) => {
        const { message } = campaign
        const { profileInfo } = profile

        return reduce(
            profileInfo,
            (result, value, key) => result.replace(`#${key}#`, value),
            message
        )
    }
)

export const selectSearchUrl = createSelector(
    selectCampaign,
    (campaign) => campaign.searchUrl
)

export const selectNonContactedProfiles = createSelector(
    selectCampaign,
    (campaign) =>
        (campaign.profiles || []).filter(
            (profile) => profile.status !== PROFILE_STATUS.CONTACTED
        )
)

export const selectAllContactedProfiles = (store) =>
    flatMap(store.campaign.campaigns, (campaign) =>
        (campaign.profiles || []).map((p) => ({
            ...p,
            campaignId: store.campaign.campaigns.indexOf(campaign),
        }))
    ).filter(({ status }) => status === PROFILE_STATUS.CONTACTED)

export const hasProfileBeenContacted = createSelector(
    selectAllContactedProfiles,
    (_, props) => props.profileUrl,
    (allContactedProfiles, profileUrl) =>
        allContactedProfiles.some(({ url }) => url === profileUrl)
)

export const hasContactedTenModuloProfiles = createSelector(
    selectAllContactedProfiles,
    (allContactedProfiles) =>
        allContactedProfiles.length > 0 && !(allContactedProfiles.length % 10)
)

export const isFirstProfileToContact = createSelector(
    selectAllContactedProfiles,
    (allContactedProfiles) => allContactedProfiles.length === 0
)

export const selectAllLoggedProfiles = createSelector(
    selectAllContactedProfiles,
    (allContactedProfiles) => allContactedProfiles.filter((p) => !!p.logged)
)

export const selectAllContactedNonLoggedProfiles = createSelector(
    selectAllContactedProfiles,
    (allContactedProfiles) => allContactedProfiles.filter((p) => !p.logged)
)

export const isCampaignRunning = createSelector(
    selectCampaign,
    (campaign) => campaign.status !== CAMPAIGN_STATUS.STOPPED
)

export const isThereAnyCampainRunning = createSelector(
    selectAllCampaigns,
    (campaigns) =>
        some(
            campaigns,
            (campaign) => campaign.status === CAMPAIGN_STATUS.RUNNING
        )
)

export const allRunningCampaigns = createSelector(
    selectAllCampaigns,
    (campaigns) => campaigns.filter((c) => c.status === CAMPAIGN_STATUS.RUNNING)
)

export const nbOfRunningCampaigns = createSelector(
    allRunningCampaigns,
    (runningCampaigns) => runningCampaigns.length
)

export const hasUserStartedAnyCampaign = createSelector(
    selectAllCampaigns,
    (campaigns) => campaigns.length > 0
)
