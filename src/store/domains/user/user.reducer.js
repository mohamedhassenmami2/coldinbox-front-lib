import { UPDATE_USER, SET_DEFAULT_MESSAGE } from './user.actionTypes'
import {
    RESYNC_STORE_WITH_STORAGE,
    RESET_ALL_STATES,
} from '../meta/meta.actionTypes'

export const initialState = {
    defaultMessage: `Hello #firstName#, I added you thanks to this awesome tool for Linkedin lead generation: ColdInbox https://www.coldinbox.com, Nice to connect !`,
}

export default function messageReducer(state = initialState, action) {
    switch (action.type) {
        case RESYNC_STORE_WITH_STORAGE:
            return {
                ...action.payload.user,
            }
        case SET_DEFAULT_MESSAGE:
            return {
                ...state,
                defaultMessage: action.payload,
            }
        case UPDATE_USER:
            return {
                ...state,
                ...action.payload,
            }
        case RESET_ALL_STATES: {
            return initialState
        }

        default:
            return state
    }
}
