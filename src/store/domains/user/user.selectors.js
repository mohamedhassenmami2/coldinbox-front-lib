import { createSelector } from 'reselect'
import { initialState } from './user.reducer'

export const selectUser = (store) => store.user

export const selectCurrentPlanMessageQuota = createSelector(
    selectUser,
    (user) => user.messageQuota
)

export const selectDefaultMessage = createSelector(
    selectUser,
    (user) => user.defaultMessage || initialState.defaultMessage
)

export const selectUserCoupon = createSelector(
    selectUser,
    (user) => user.coupon
)
