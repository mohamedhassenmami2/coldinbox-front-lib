import { createSelector } from 'reselect'
import find from 'lodash/find'
import { selectUserCoupon } from '../user/user.selectors'

export const selectPlans = createSelector(
    (store) => store.plan.plans,
    selectUserCoupon,
    (plans, coupon) =>
        coupon
            ? plans.map((p) => ({
                  ...p,
                  monthlyPrice:
                      (p.monthlyPrice * (100 - coupon.percent_off)) / 100,
              }))
            : plans
)

export const selectCurrentPlan = createSelector(
    selectPlans,
    (store) => store.plan.currentPlanId,
    (plans, currentPlanId) => find(plans, { id: currentPlanId })
)
