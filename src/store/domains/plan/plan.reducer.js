import { SET_PLANS, SET_CURRENT_PLAN } from './plan.actionTypes'
import { RESYNC_STORE_WITH_STORAGE } from '../meta/meta.actionTypes'

export const initialState = {
    plans: [],
    currentPlanId: '',
}

export default function planReducer(state = initialState, action) {
    switch (action.type) {
        case RESYNC_STORE_WITH_STORAGE:
            return {
                ...action.payload.plan,
            }
        case SET_PLANS:
            return {
                ...state,
                plans: action.payload,
            }

        case SET_CURRENT_PLAN: {
            return {
                ...state,
                currentPlanId: action.payload,
            }
        }

        default:
            return state
    }
}
