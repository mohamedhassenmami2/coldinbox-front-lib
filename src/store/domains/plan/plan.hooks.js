import { useFetchOnce } from '../../../utils/useFetchOnce'
import { selectCurrentPlan, selectPlans } from './plan.selectors'
import { fetchAndSelectPlanById, fetchAllPlans } from './plan.actionCreators'

export const useFetchPlan = useFetchOnce(
    fetchAndSelectPlanById,
    selectCurrentPlan,
    true
)

export const useFetchAllPlans = useFetchOnce(fetchAllPlans, selectPlans, true)
