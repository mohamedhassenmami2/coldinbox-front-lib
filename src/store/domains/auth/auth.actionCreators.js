import AuthService from '../../../clientApi/AuthService'
import { updateUser } from '../user/user.actionCreators'

export const signUp = (email, password) => async (dispatch, _) => {
    const user = await AuthService.signUp(email, password)

    updateUser(user)(dispatch, _)
}

export const logIn = (email, password) => async (dispatch, _) => {
    const user = await AuthService.logIn(email, password)

    updateUser(user)(dispatch, _)
}
