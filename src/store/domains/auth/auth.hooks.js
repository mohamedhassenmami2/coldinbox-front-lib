import { useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { resetAll } from '../../store.actionCreators'
import { isAuthenticated } from './auth.selectors'
import { useAction } from '../../../utils/hooks'
import { isThereAnyCampainRunning } from '../campaign/campaign.selectors'

export const useResetStore = () => {
    const execResetAll = useAction(resetAll)
    const location = useHistory()

    useEffect(() => {
        if (
            ['/ext-tabs/login', '/ext-tabs/sign-up'].includes(location.pathname)
        ) {
            execResetAll()
        }
    }, [location])
}

export const useRedirectIfNonAuthenticated = () => {
    const history = useHistory()
    const authenticated = useSelector(isAuthenticated)
    const anyCampaignRunning = useSelector(isThereAnyCampainRunning)

    useEffect(() => {
        if (!authenticated) {
            return history.push('/ext-tabs/sign-up')
        }

        if (history.location.pathname === '/') {
            if (anyCampaignRunning) {
                return history.push('/ext-tabs/activity')
            } else {
                return history.push('/ext-tabs/message')
            }
        }

        // let the current location
    }, [authenticated])
}

export const useIsProtectedRoute = () => {
    const location = useLocation()
    const [isProtectedRoute, setIsProtectedRoute] = useState()

    useEffect(() => {
        setIsProtectedRoute(
            ['/ext-tabs/login', '/ext-tabs/sign-up'].includes(location.pathname)
        )
    }, [location])

    return isProtectedRoute
}
