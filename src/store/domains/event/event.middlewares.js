import noop from 'lodash/noop'
import { postFormattedEvent } from './event.actionCreators'
import {
    ONBOARDING_WELCOME_ACTIVITY,
    ONBOARDING_WELCOME_DESCRIPTION,
    ONBOARDING_EXPLAIN_TITLE,
    ONBOARDING_EXPLAIN_SUBTITLE,
    ONBOARDING_EXPLAIN_LOAD_PROFILES,
    ONBOARDING_EXPLAIN_HIGHLIGHT_PROFILE,
    ONBOARDING_EXPLAIN_OPEN_TAB,
    ONBOARDING_EXPLAIN_CLOSE_WINDOW,
    ONBOARDING_EXPLAIN_WAIT,
    ONBOARDING_EXPLAIN_REOPEN,
    WAIT_EVENT,
    POST_RAW_EVENT,
    FIRST_PAGE_LOADED,
    ZERO_CREDITS_REMAINING,
    GOTO_NEXT_PAGE,
    LAST_PAGE,
    LOADING_PROFILES_ON_PAGE,
    FINISHED_LOADING_ALL_PROFILES,
    PROFILE_LOADED,
    SUCCESSFUL_SENT_INVITATION_TO_PROFILE,
    FINISHED_CONTACTED_PROFILE,
    CLOSE_TAB_PROFILE,
    LOADING_MESSAGE_WIDGET,
    WRITING_MESSAGE_TO_PROFILE,
    NEXT_PAGE_LOADED,
    PROFILE_INFO_CRAWLED,
    PROFILE_INFO_START_CRAWLING,
    ERROR_PROFILE_ALREADY_SENT_INVITATION_ON_WAIT,
    FAILED_SENDING_INVITATION_TO_PROFILE,
    ERROR_CAMPAIGN_STOPPED,
} from './event.actionTypes'
import { invokeActionCreator } from '../../../utils/reduxUtils'
import { ONE_MINUTE, ONE_SECOND } from '../../../utils/time'
import { PAUSE, INFO, WARNING, PUSH, SUCCESS, ERROR } from './event.types'
import {
    LAUNCH_NEW_CAMPAIGN,
    SET_CAMPAIGN_STATUS_FINISHED,
    SET_ALL_CAMPAIGNS_STATUS_STOPPED,
} from '../campaign/campaign.actionTypes'

const actionToEvent = {
    [ONBOARDING_WELCOME_ACTIVITY]: () => ({
        type: SUCCESS,
        message: `Welcome to your activity board !`,
    }),
    [ONBOARDING_WELCOME_DESCRIPTION]: () => ({
        type: INFO,
        message: `We thought this was the best way to keep you informed about what's going on`,
    }),
    [ONBOARDING_EXPLAIN_TITLE]: () => ({
        type: INFO,
        message: `As this is your first time, we will take time to explain you few things`,
    }),
    [ONBOARDING_EXPLAIN_SUBTITLE]: () => ({
        type: INFO,
        message: 'Whenever you launch a campaign multiple things happens:',
    }),
    [ONBOARDING_EXPLAIN_LOAD_PROFILES]: () => ({
        type: INFO,
        message:
            '1 - We load all results of your linkedin search, page by page',
    }),
    [ONBOARDING_EXPLAIN_HIGHLIGHT_PROFILE]: () => ({
        type: INFO,
        message:
            '2 - We hightlight the current profile with a green background',
    }),
    [ONBOARDING_EXPLAIN_OPEN_TAB]: () => ({
        type: INFO,
        message:
            '3 - We will open a new Linkedin tab in your browser. We will send the Linkedin invitation message from this tab.',
    }),
    [ONBOARDING_EXPLAIN_CLOSE_WINDOW]: () => ({
        type: WARNING,
        message:
            'Inevitably the ColdInbox popup will close, you can reopen it at any time !',
    }),
    [ONBOARDING_EXPLAIN_REOPEN]: () => ({
        type: INFO,
        message: `Next time your campaign will launch way faster !`,
    }),
    [ONBOARDING_EXPLAIN_WAIT]: () => ({
        type: INFO,
        message:
            'We pause ColdInbox multiple times to emulate best the user journey on LinkedIn',
    }),
    [LAUNCH_NEW_CAMPAIGN]: () => ({
        type: INFO,
        message: 'A new campaign has been started',
    }),
    [SET_CAMPAIGN_STATUS_FINISHED]: () => ({
        type: SUCCESS,
        message: 'Campaign successfully finished !',
    }),
    [SET_ALL_CAMPAIGNS_STATUS_STOPPED]: () => ({
        type: WARNING,
        message: `You stopped all of your campaigns ! All tabs will be closed soon`,
    }),
    [WAIT_EVENT]: (_, { waitTime }) => ({
        type: PAUSE,
        message: `Pause for ${
            waitTime >= ONE_MINUTE
                ? waitTime / ONE_MINUTE
                : waitTime / ONE_SECOND
        } ${waitTime >= ONE_MINUTE ? 'minutes' : 'secondes'}`,
    }),
    [FIRST_PAGE_LOADED]: () => ({
        type: INFO,
        message: 'First page has been successfully loaded',
    }),
    [GOTO_NEXT_PAGE]: () => ({
        type: INFO,
        message: 'Heading to the next page...',
    }),
    [NEXT_PAGE_LOADED]: () => ({
        type: INFO,
        message: 'Successfully loaded next page',
    }),
    [LAST_PAGE]: () => ({
        type: WARNING,
        message: 'Loading the last page, campaign will finish soon',
    }),
    [ZERO_CREDITS_REMAINING]: () => ({
        type: PUSH,
        message: `Congratulations ! You have contacted 5 persons thanks to ColdInbox ! But you don't have credits anymore. But no worries, you can buy some here`,
    }),
    [LOADING_PROFILES_ON_PAGE]: () => ({
        type: INFO,
        message: `Scrolling to load all profiles on page`,
    }),
    [FINISHED_LOADING_ALL_PROFILES]: (_, { totalProfiles }) => ({
        type: INFO,
        message: `Successfully loaded ${totalProfiles} profiles on page`,
    }),
    [PROFILE_LOADED]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `Loading Linkedin of profile ${profileUrl}`,
    }),
    [PROFILE_INFO_START_CRAWLING]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `Start extracting personal information for ${profileUrl}`,
    }),
    [PROFILE_INFO_CRAWLED]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `Successfully extracted all necessary info of ${profileUrl}`,
    }),
    [LOADING_MESSAGE_WIDGET]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `Loading chat widget for ${profileUrl} ...`,
    }),
    [WRITING_MESSAGE_TO_PROFILE]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `Writing message to ${profileUrl} ...`,
    }),
    [SUCCESSFUL_SENT_INVITATION_TO_PROFILE]: (
        _,
        { url: profileUrl, message }
    ) => ({
        type: INFO,
        message: `Just wrote the following invitation message to ${profileUrl}:
        ${message}`,
    }),
    [FAILED_SENDING_INVITATION_TO_PROFILE]: (_, { url: profileUrl }) => ({
        type: WARNING,
        message: `We were not able to send an invitation to ${profileUrl}`,
    }),
    [FINISHED_CONTACTED_PROFILE]: (_, { url: profileUrl }) => ({
        type: SUCCESS,
        message: `Successfully sent an invitaton to ${profileUrl}`,
    }),
    [CLOSE_TAB_PROFILE]: (_, { url: profileUrl }) => ({
        type: INFO,
        message: `close tab of profile ${profileUrl}`,
    }),
    [ERROR_PROFILE_ALREADY_SENT_INVITATION_ON_WAIT]: (_, profile) => ({
        type: WARNING,
        message: `It seems like you have already invited ${
            profile.profileInfo ? profile.profileInfo.fullName : profile.url
        }, we will just skip this profile`,
    }),
    [ERROR_CAMPAIGN_STOPPED]: () => ({
        type: ERROR,
        message: `Something went wrong with your campaign. Try to restart it.
        Don't worry, we already have been notified.
        Send an email to support@coldinbox.com for keeping up with the progress of the bug resolution.`,
    }),
}

const interceptRawEventsFormatAndPost = (store) => (next) => async (action) => {
    const r = next(action)

    switch (action.type) {
        case LAUNCH_NEW_CAMPAIGN:
        case SET_CAMPAIGN_STATUS_FINISHED:
        case SET_ALL_CAMPAIGNS_STATUS_STOPPED: {
            const formattedEvent = (actionToEvent[action.type] || noop)(
                store,
                action.payload
            )

            if (formattedEvent) {
                invokeActionCreator(store)(postFormattedEvent)(formattedEvent)
            }

            break
        }

        case POST_RAW_EVENT: {
            const { type, payload } = action.payload || {}

            const formattedEvent = (actionToEvent[type] || noop)(store, payload)

            if (formattedEvent) {
                invokeActionCreator(store)(postFormattedEvent)(formattedEvent)
            }
            break
        }
        default: {
        }
    }

    return r
}

export default [interceptRawEventsFormatAndPost]
