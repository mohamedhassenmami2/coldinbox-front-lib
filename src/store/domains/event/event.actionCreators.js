import {
    POST_FORMATTED_EVENT,
    POST_RAW_EVENT,
    WAIT_EVENT,
    ONBOARDING_EXPLAIN_WAIT,
} from './event.actionTypes'
import { bindActionCreator, bindSelector } from '../../../utils/reduxUtils'
import { wait, ONE_SECOND } from '../../../utils/time'
import { hasNeverLoggedPauseEvents } from './event.selectors'

export const postRawEvent = ({ type, payload }) => (dispatch, _) =>
    dispatch({
        type: POST_RAW_EVENT,
        payload: { type, payload },
    })

export const postFormattedEvent = (event) => (dispatch, _) =>
    dispatch({
        type: POST_FORMATTED_EVENT,
        payload: event,
    })

export const notifyAndWait = (waitTime) => {
    if (waitTime >= 30 * ONE_SECOND) {
        try {
            const hasNeverPaused = bindSelector(hasNeverLoggedPauseEvents)()

            logEvent(WAIT_EVENT, { waitTime })

            if (hasNeverPaused) {
                logEvent(ONBOARDING_EXPLAIN_WAIT, { waitTime })
            }
        } catch (err) {
            console.error(err)
        }
    }

    return wait(waitTime)
}

export const logEvent = (type, payload) =>
    bindActionCreator(postRawEvent)({ type, payload })
