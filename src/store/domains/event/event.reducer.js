import { POST_FORMATTED_EVENT } from './event.actionTypes'
import { RESYNC_STORE_WITH_STORAGE } from '../meta/meta.actionTypes'

export const initialState = {
    events: [],
}

export default function eventReducer(state = initialState, action) {
    switch (action.type) {
        case RESYNC_STORE_WITH_STORAGE:
            return {
                ...action.payload.event,
            }

        case POST_FORMATTED_EVENT:
            return {
                ...state,
                events: [...state.events, action.payload],
            }

        default:
            return state
    }
}
