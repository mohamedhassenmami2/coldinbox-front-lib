import React, { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { NavLink } from 'react-router-dom'
import HelpIcon from '@material-ui/icons/Help'
import HomeIcon from '@material-ui/icons/Home'
import EmailIcon from '@material-ui/icons/Email'
import PaymentIcon from '@material-ui/icons/Payment'

const StyledLink = styled(NavLink)`
    box-sizing: border-box;
    padding: 23px;
    text-decoration: none;
    display: inline-block;
    border: 1px ${(props) => props.theme.color_7} solid;
    color: ${(props) => props.theme.color_3};
    width: 95px;

    display: flex;
    flex-direction: column;
    align-items: center;

    &:hover {
        background-color: ${(props) => props.theme.color_7};
        border-left: 5px ${(props) => props.theme.color_1} solid;
    }
`

export default function NavBar() {
    const theme = useContext(ThemeContext)

    const activeStyle = {
        backgroundColor: theme.color_2,
        borderLeft: `5px ${theme.color_1} solid`,
        borderRight: `none`,
    }

    return (
        <nav>
            <ul>
                <li>
                    <StyledLink
                        exact
                        to="/ext-tabs/activity"
                        activeStyle={activeStyle}
                    >
                        <HomeIcon fontSize="large" />
                        <span>Activity</span>
                    </StyledLink>
                </li>
                <li>
                    <StyledLink
                        to="/ext-tabs/message"
                        activeStyle={activeStyle}
                    >
                        <EmailIcon fontSize="large" />
                        <span>Message</span>
                    </StyledLink>
                </li>
                <li>
                    <StyledLink to="/ext-tabs/plans" activeStyle={activeStyle}>
                        <PaymentIcon fontSize="large" />
                        <span>Plans</span>
                    </StyledLink>
                </li>
                <li>
                    <StyledLink to="/ext-tabs/help" activeStyle={activeStyle}>
                        <HelpIcon fontSize="large" />
                        <span>Help</span>
                    </StyledLink>
                </li>
            </ul>
        </nav>
    )
}
