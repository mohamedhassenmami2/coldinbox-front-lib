import React from 'react'
import styled from 'styled-components'
import NavBar from './NavBar'
import { useMessageQuota } from '../store/domains/user/user.hooks'

const BodyCtn = styled.div`
    display: flex;
`

const NavBarCtn = styled.div`
    flex: 1;
`
const PageCtn = styled.div`
    flex: 7;
    display: flex;
    flex-direction: column;
`

const HeaderCtn = styled.div`
    flex: 1;
    padding-left: 10px;
    padding-top: 30px;
    padding-right: 10px;
    display: flex;
    justify-content: space-between;
`

const PageBodyCtn = styled.div`
    flex: 6;
    padding: 10px;
    background: ${(props) => props.theme.color_2};
`

const TitlePlaceholder = styled.h1`
    font-size: 40px;
    color: ${(props) => props.theme.color_1};
`
const MessagePlaceholder = styled.span`
    border-radius: 8px;
    background: ${(props) => props.theme.color_1};
    padding: 5px 10px;
    color: white;
`

function Header({ title }) {
    const [messageQuota] = useMessageQuota()

    return (
        <React.Fragment>
            <TitlePlaceholder>{title}</TitlePlaceholder>
            <div>
                <MessagePlaceholder>{messageQuota} credits</MessagePlaceholder>
            </div>
        </React.Fragment>
    )
}

const FooterCtn = styled.div`
    width: 100%;
    height: 95px;
    background-color: ${(props) => props.theme.color_1};
`

const ButtonCtn = styled.div`
    position: absolute;
    right: 20px;
    bottom: 30px;
`

export default function PageLayout({ children, title, button }) {
    return (
        <div>
            <BodyCtn>
                <NavBarCtn>
                    <NavBar />
                </NavBarCtn>
                <PageCtn>
                    <HeaderCtn>
                        <Header title={title} />
                    </HeaderCtn>
                    <PageBodyCtn>{children}</PageBodyCtn>
                </PageCtn>
            </BodyCtn>
            <FooterCtn>{button && <ButtonCtn>{button}</ButtonCtn>}</FooterCtn>
        </div>
    )
}
