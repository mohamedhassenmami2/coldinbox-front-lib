import React from 'react'
import MuiButton from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

const StyledButton = withStyles({
    root: {
        backgroundColor: '#f8cb47',
        border: 0,
        '&:hover': {
            backgroundColor: '#d2ab3b',
        },
    },
    label: {
        textTransform: 'initial',
        color: 'black',
    },
})(MuiButton)

export default function AuthButton(props) {
    return <StyledButton variant="outlined" {...props} />
}
