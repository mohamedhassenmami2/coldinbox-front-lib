import React from 'react'
import ReactTooltip from 'react-tooltip'
import styled from 'styled-components'

const TooltipPlaceholder = styled.p`
    display: inline-block;
    cursor: pointer;
    font-weight: bold;
    color: ${props => props.theme.color_6};
`

export default function Tooltip({ placeholder, message }) {
    return (
        <React.Fragment>
            <ReactTooltip multiline={true} place="top" effect={'solid'} />
            <TooltipPlaceholder data-tip={message}>
                {placeholder}{' '}
            </TooltipPlaceholder>
        </React.Fragment>
    )
}
