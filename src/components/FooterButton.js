import React from 'react'
import MuiButton from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

const StyledButton = withStyles({
    root: {
        backgroundColor: 'white',
        border: 0,
        '&:hover': {
            backgroundColor: '#f9f9f9',
        },
    },
    label: {
        textTransform: 'initial',
        color: '#509CF1',
        fontWeight: 'bold',
        fontSize: '15px',
    },
})(MuiButton)

export default function FooterButton(props) {
    return <StyledButton variant="outlined" {...props} />
}
