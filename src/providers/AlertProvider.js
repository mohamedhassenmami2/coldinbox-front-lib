import React from 'react'
import { positions, Provider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

const options = {
    timeout: 5000,
    position: positions.BOTTOM_CENTER,
}

export default function AlertProvider({ children }) {
    return (
        <Provider template={AlertTemplate} {...options}>
            {children}
        </Provider>
    )
}
