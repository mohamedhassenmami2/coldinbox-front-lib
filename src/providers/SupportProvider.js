import { useEffect } from 'react'
import { useIsProtectedRoute } from '../store/domains/auth/auth.hooks'

export default function SupportProvider({ children }) {
    const isProtectedRoute = useIsProtectedRoute()

    useEffect(() => {
        if (isProtectedRoute !== false) {
            return
        }

        ;(function(t, a, l, k, u, s, e) {
            if (!t[u]) {
                ;(t[u] = function() {
                    ;(t[u].q = t[u].q || []).push(arguments)
                }),
                    (t[u].l = 1 * new Date())
                ;(s = a.createElement(l)), (e = a.getElementsByTagName(l)[0])
                s.async = 1
                s.src = k
                e.parentNode.insertBefore(s, e)
            }
        })(
            window,
            document,
            'script',
            'https://talkus.io/plugin.beta.js',
            'talkus'
        )

        talkus('init', 'eCRsbvdmm647JcD6F')
    }, [isProtectedRoute])

    return children
}
