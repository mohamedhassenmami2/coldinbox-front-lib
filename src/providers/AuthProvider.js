import {
    useResetStore,
    useRedirectIfNonAuthenticated,
} from '../store/domains/auth/auth.hooks'

export default function AuthProvider({ children }) {
    useResetStore()
    useRedirectIfNonAuthenticated()

    return children
}
