import React from 'react'
import StyleProvider from './StyleProvider'
import AlertProvider from './AlertProvider'
import PaymentProvider from './PaymentProvider'
import AuthProvider from './AuthProvider'
import AnalyticsProvider from './AnalyticsProvider'

export default function Providers({ children }) {
    return (
        <AnalyticsProvider>
            <AuthProvider>
                <PaymentProvider>
                    <StyleProvider>
                        <AlertProvider>{children}</AlertProvider>
                    </StyleProvider>
                </PaymentProvider>
            </AuthProvider>
        </AnalyticsProvider>
    )
}
